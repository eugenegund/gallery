from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .forms import UserRegistrationForm
from django.contrib.auth.views import LoginView, LogoutView


class LoginUserView(LoginView):
    template_name = 'account/registration/login.html'
    next = reverse_lazy('show:index')
    redirect_authenticated_us = True
    success_url = reverse_lazy('show:index')


class RegistrationView(CreateView):

    template_name = 'account/registration/register.html'
    form_class = UserRegistrationForm
    success_url = reverse_lazy('account:login')


class LogoutUserView(LogoutView):
    next_page = reverse_lazy('show:index')