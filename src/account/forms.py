from django.forms import ModelForm, CharField, PasswordInput
from django.contrib.auth.models import User


class UserRegistrationForm(ModelForm):
    password = CharField(label='Пароль', widget=PasswordInput)

    def save(self, commit=False):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user

    class Meta:
        model = User
        fields = ('username',)