from django.db import models
from django.contrib.auth.models import User


class Image(models.Model):
    photo = models.ImageField(upload_to='')
    description = models.TextField(default='')
    pub_date = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата добавления')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def user_like_it(self, user):
        return bool(Like.objects.filter(image=self, user=user))

    @property
    def url(self):
        if self.photo and hasattr(self.photo, 'url'):
            return self.photo.url

    @property
    def likes_count(self):
        return self.like_set.count()

    @property
    def comments_count(self):
        return self.comment_set.count()

    def __str__(self):
        return f'image: {self.description}'


class Like(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE, verbose_name='Изображение')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')

    class Meta:
        unique_together = ('image', 'user')


class Comment(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE, verbose_name='Изображение')
    user = models.CharField(max_length=30, verbose_name='Автор')
    content = models.TextField(verbose_name='Комментарий')
    created_date = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата создания')

