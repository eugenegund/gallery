from django.urls import path
from . import views


urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.ImageDetailView.as_view(), name='detail'),
    path('add/', views.ImageCreateView.as_view(), name='add'),
    path('<int:pk>/like/', views.LikeView.as_view(), name='like'),
    path('<int:pk>/comment/', views.CommentView.as_view(), name='comment')
]