from django.views.generic.edit import FormMixin
from django.http import HttpResponse


class ComponentsImageFormMixin(FormMixin):

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponse('OK')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = {'user': self.request.user, 'image': self.get_object()}
        return kwargs
