from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, ProcessFormView
from django.urls import reverse_lazy
from .models import Image, Like, Comment
from .forms import ImageForm, LikeSetForm, CommentForm
from .utils import ComponentsImageFormMixin


class IndexView(LoginRequiredMixin, ListView):

    template_name = 'show/index.html'
    login_url = 'account:login'
    paginate_by = 3 * 2
    model = Image

    def get_queryset(self):
        images = Image.objects.all()
        pressed_status = []
        for image in images:
            pressed_status.append(image.user_like_it(user=self.request.user))

        return list(zip(images, pressed_status))


class ImageDetailView(LoginRequiredMixin, DetailView, ComponentsImageFormMixin):
    model = Image
    template_name = 'show/detail.html'
    form_class = CommentForm

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        image = self.object

        context['has_like'] = image.user_like_it(user=self.request.user)
        context['page_number'] = self.request.GET.get('page', None)
        context['comments'] = Comment.objects.filter(image=self.get_object().pk)
        return context


class CommentView(LoginRequiredMixin, ProcessFormView, SingleObjectMixin, ComponentsImageFormMixin):
    model = Image
    form_class = CommentForm


class ImageCreateView(LoginRequiredMixin, CreateView):
    template_name = 'show/create.html'
    form_class = ImageForm
    success_url = reverse_lazy('show:index')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'].update({'user': self.request.user.pk, })
        return kwargs


class LikeView(LoginRequiredMixin, ProcessFormView, SingleObjectMixin, ComponentsImageFormMixin):
    model = Image
    form_class = LikeSetForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['data'] = {'user': self.request.user, 'image': self.get_object()}
        return kwargs

    def delete(self, request, *args, **kwargs):
        image = self.get_object()
        like = Like.objects.filter(user=self.request.user.id, image=image.id).first()
        if like:
            like.delete()
        return HttpResponse('OK')


