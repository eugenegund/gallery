from django.forms import ModelForm, HiddenInput
from .models import Image, Like, Comment


class ImageForm(ModelForm):

    class Meta:
        model = Image
        widgets = {'user': HiddenInput}
        fields = {'user', 'description', 'photo'}


class LikeSetForm(ModelForm):

    class Meta:
        model = Like
        fields = {'user', 'image'}


class CommentForm(ModelForm):

    class Meta:
        model = Comment
        widgets = {'user': HiddenInput, 'image': HiddenInput}
        fields = {'content', 'user', 'image'}
