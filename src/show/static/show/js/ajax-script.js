 $(document).ready(function()
 {
    function getCookie(name)
    {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '')
        {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++)
            {
                var cookie = jQuery.trim(cookies[i]);

                if (cookie.substring(0, name.length + 1) === (name + '='))
                {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method)
    {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false,
        beforeSend: function(xhr, settings)
        {
            if (!csrfSafeMethod(settings.type))
            {
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }
        }
    });



    $('.imagecomment').on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            data = $this.data();

        form = $("#new_comment_form");

        var $data_form = {};
        form.find('textarea').each(function() {
            $data_form[this.name] = $(this).attr('id');
        });

        comment = $(`#${$data_form.content}`).val();

        if (comment === "")
        {
            $("#id_content").focus();
            return false;
        }

        user = data.user;
        comments = $('#comments').html();

        $.ajax({
            url: data.url,
            method: data.method,
            dataType: 'html',
            data: form.serialize(),

            success: function(data)
            {
                if (comments.trim() == '<h5>Комментарий нет</h5>')
                {
                    comments = '';
                }
                $('#comments').html(comments + `<div><h5>${user}</h5><p>${comment}</p></div>`);
                $("#id_content").val('');
            },

        });
    });



    $('.imagelike').on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            data = $this.data();

        image_id = data.image_id
        count_likes = Number($('#count_likes'+'_' + data.image_id).html())
        button_like = $('#like' + data.image_id)[0];
        href = button_like.href;

        url = (new URL(href).pathname) + 'like/';
        if (!button_like.classList.contains('pressed'))
        {
            count_likes = count_likes + 1
            button_like.classList.add('pressed')
            method = 'POST'
        }
        else
        {
            method = 'DELETE'
            count_likes = count_likes - 1
            button_like.classList.remove('pressed')
        }

        $.ajax({
            url: url,
            method: method,
            data: data,

            success: function(data)
            {
                $('#count_likes'+'_' + image_id).html(count_likes);
            }

        });
    });
});